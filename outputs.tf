output "project" {
    value = google_compute_instance.vm_instance[0].project
}

output "hostnames" {
    value = google_compute_instance.vm_instance.*.name
}