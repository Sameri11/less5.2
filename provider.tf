terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

provider "google" {
  project = "teak-droplet-278507"
  region  = "us-central1"
  zone    = "us-central1-c"
}