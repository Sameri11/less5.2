resource "google_compute_instance" "vm_instance" {
  count = "${var.web_instance_count}"
  name = "node-${count.index + 1}"
  machine_type = "e2-small"

  connection {
    type     = "ssh"
    user     = "${var.remote_user}"
    private_key = "${file(var.remote_key)}"
    host     = "${self.network_interface.0.access_config.0.nat_ip}"
  }

  provisioner "local-exec" {
    command = "echo '${self.name} ${self.network_interface.0.access_config.0.nat_ip}' >> host.list"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install nginx -y",
      "echo 'Juneway ${self.network_interface.0.access_config.0.nat_ip} ${self.name}' | sudo tee /var/www/html/index.nginx-debian.html"
    ]
  }

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
      size = 20
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}

resource "google_compute_firewall" "tcp" {
  name = "tcp"
  network = "default"

  allow {
    protocol = "tcp"
    ports = ["433", "80"]
  }

  source_ranges = [ "0.0.0.0/0" ]
}


resource "google_compute_firewall" "udp" {
  name = "udp"
  network = "default"

  allow {
    protocol = "udp"
    ports = ["10000-20000"]
  }

  source_ranges = [ "10.0.0.23" ]
}